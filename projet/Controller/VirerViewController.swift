//
//  VirerViewController.swift
//  projet
//
//  Created by Hang Nga on 03/01/2021.
//

import UIKit
import CoreData
import Foundation

class VirerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var txtCompteDebiter: UITextField!
    @IBOutlet weak var txtMontant: UITextField!
    @IBOutlet weak var pickerTextField : UITextField!
    
    public var completionHandler: (() -> Void)?
    
    public var selectedTypeCompte: Int = 1
    
    private var arrData = [ClientComptesModel]()
    
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
    
    // argent only input number
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = ".1234567890" // Float
        let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
        let typedCharacterSet = CharacterSet(charactersIn: string)
        
        return allowedCharacterSet.isSuperset(of: typedCharacterSet)
    }
    
    var typeCompteLivreta = ["", "Compte Courant", "Compte Épargne"]
    var typeCompteEpargne = ["", "Compte Courant", "Compte Livret A"]
    var typeCompteCourant = ["", "Compte Livret A", "Compte Épargne"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // get user_defaut: marked logined
        let defaults = UserDefaults.standard
        let idUser = defaults.value(forKey: "idUser")
        if idUser as! Int == 0 {
            didTapHome()
        }
        // end get user_defaut: marked logined
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerTextField.inputView = pickerView
        
        if selectedTypeCompte == 2 {
            txtCompteDebiter.text = "Compte Livret A"
        } else if selectedTypeCompte == 3 {
            txtCompteDebiter.text = "Compte Épargne"
        } else {
            txtCompteDebiter.text = "Compte Courant"
        }
        txtCompteDebiter.isUserInteractionEnabled = false
        
        // no show title for button back
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // argent only input number
        txtMontant.delegate = self
        
        // test have how many comptes
        arrData = ClientComptesData.getAllClientComptesData()
        var strCourant = ""
        var strCompteLivreta = ""
        var strCompteEpargne = ""
        
        for result in arrData {
            if result.titleNomComptes == "courant" {
                strCourant = "Compte Courant"
            }
            if result.titleNomComptes == "livreta" {
                strCompteLivreta = "Compte Livret A"
            }
            if result.titleNomComptes == "epargne" {
                strCompteEpargne = "Compte Épargne"
            }
        }
        
        typeCompteLivreta = ["", "\(strCourant)", "\(strCompteEpargne)"]
        typeCompteEpargne = ["", "\(strCourant)", "\(strCompteLivreta)"]
        typeCompteCourant = ["", "\(strCompteLivreta)", "\(strCompteEpargne)"]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedTypeCompte == 2 {
            return typeCompteLivreta.count
        } else if selectedTypeCompte == 3 {
            return typeCompteEpargne.count
        } else {
            return typeCompteCourant.count
        }
    }
    
    // This function sets the text of the picker view to the content of the "salutations" array
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedTypeCompte == 2 {
            return typeCompteLivreta[row]
        } else if selectedTypeCompte == 3 {
            return typeCompteEpargne[row]
        } else {
            return typeCompteCourant[row]
        }
    }
    
    // When user selects an option, this function will set the text of the text field to reflect
    // the selected option.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedTypeCompte == 2 {
            pickerTextField.text = typeCompteLivreta[row]
        } else if selectedTypeCompte == 3 {
            pickerTextField.text = typeCompteEpargne[row]
        } else {
            pickerTextField.text = typeCompteCourant[row]
        }
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        
        let lblArgent = txtMontant.text!
        let lblCompteCrediter = pickerTextField.text!
        
        if lblArgent.isEmpty || lblCompteCrediter.isEmpty {
            displayMyAlertMessage(userMessage: "Vous devez saisir le montant et compte créditer")
            return
        } else {
        
            let textSelected = pickerTextField.text as! String
            var typeCompteSelected = 1
            var varNomRecu = "courant"
            var varEntityNameRecu = "CompteCourant"
            
            if textSelected == "Compte Épargne" {
                typeCompteSelected = 3
            } else if textSelected == "Compte Livret A" {
                typeCompteSelected = 2
            } else {
                typeCompteSelected = 1
            }
            
            if typeCompteSelected == 1 {
                varNomRecu = "courant"
                varEntityNameRecu = "CompteCourant"
            } else if typeCompteSelected == 2 {
                varNomRecu = "livreta"
                varEntityNameRecu = "CompteLivretA"
            } else {
                varNomRecu = "epargne"
                varEntityNameRecu = "CompteEpargne"
            }
            
            let typeCompte = selectedTypeCompte
            
            // get id User
            let defaults = UserDefaults.standard
            let idUserLogined = defaults.value(forKey: "idUser") as! Int
            // end get id User
            
            var varEntityName = "CompteCourant"
            var varNom = "courant"
            
            if typeCompte == 1 {
                varEntityName = "CompteCourant"
                varNom = "courant"
            } else if typeCompte == 2 {
                varEntityName = "CompteLivretA"
                varNom = "livreta"
            } else {
                varEntityName = "CompteEpargne"
                varNom = "epargne"
            }
            
            // connect db
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
            
            // get data
            var id_retirer = 0
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: varEntityName)
            request.returnsObjectsAsFaults = false
            request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
            request.fetchLimit = 1
            do {
                let result = try context.fetch(request) as! [NSManagedObject]
                id_retirer = result[0].value(forKey: "id") as! Int
            }
            catch let error as NSError {
                print("\(error)")
            }
            // end get data to get id
            
            // get data from table client to sum money
            let id_client = idUserLogined
            var argent_solde:Float = 0
            var argent_compte_recu_solde:Float = 0
            let requestSolde = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
            requestSolde.returnsObjectsAsFaults = false
            let predicate = NSPredicate(format: "id = %i", id_client)
            requestSolde.predicate = predicate
            do {
                let result = try context.fetch(requestSolde) as! [NSManagedObject]
                argent_solde = result[0].value(forKey: "compte_\(varNom)_solde") as! Float
                
                // compte recu quand vire
                argent_compte_recu_solde = result[0].value(forKey: "compte_\(varNomRecu)_solde") as! Float
            }
            catch let error as NSError {
                print("\(error)")
            }
            // end get data from table client to sum money
            
            // update money in table client
            var argentCaculated:Float = 0
            var argentCaculatedRecu:Float = 0
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Client")
            let id_1 = idUserLogined
            let predicateClient = NSPredicate(format: "id = %i", id_1)
            fetchRequest.predicate = predicateClient
            do {
                let  rs = try context.fetch(fetchRequest)
                for result in rs as [NSManagedObject] {
                    // update
                    do {
                        let managedObject = rs[0]
                        argentCaculated = argent_solde - Float(txtMontant.text ?? "0")!
                        argentCaculatedRecu = argent_compte_recu_solde + Float(txtMontant.text ?? "0")!
                        
                        // test argent < 10: compte livreta
                        if argentCaculated < 0 {
                            displayMyAlertMessage(userMessage: "Le montant que vous transférez est supérieur à votre solde actuel")
                            return
                        } else {
                            if (argentCaculated < 10) && (typeCompte == 2) {
                                displayMyConfirmMessage(userMessage: "Ce compte doit être d'au moins 10 euros, sinon le compte sera fermé.", appDel: appDel, context: context, idUserLogined: idUserLogined, typeCompte: typeCompte, argentCaculated: argentCaculated, varNom: varNom, argentCaculatedRecu: argentCaculatedRecu, varNomRecu: varNomRecu, managedObject: managedObject, varEntityName: varEntityName, typeCompteSelected: typeCompteSelected, varEntityNameRecu: varEntityNameRecu, id_retirer: id_retirer)
                                return
                            } else if (argentCaculated == 0) && (typeCompte == 3) {
                                displayMyConfirmMessage(userMessage: "L'argent de ce compte est égal à 0, le compte sera fermé.", appDel: appDel, context: context, idUserLogined: idUserLogined, typeCompte: typeCompte, argentCaculated: argentCaculated, varNom: varNom, argentCaculatedRecu: argentCaculatedRecu, varNomRecu: varNomRecu, managedObject: managedObject, varEntityName: varEntityName, typeCompteSelected: typeCompteSelected, varEntityNameRecu: varEntityNameRecu, id_retirer: id_retirer)
                                return
                            }
                        }
                        // end test argent < 10
                        
                        updateArgentClient(argentCaculated: argentCaculated, varNom: varNom, argentCaculatedRecu: argentCaculatedRecu, varNomRecu: varNomRecu, managedObject: managedObject)
                        
                        try context.save()
                        print("update successfull")
                        
                    } catch let error as NSError {
                        print("Could not Update. \(error), \(error.userInfo)")
                    }
                    //end update
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            // end update money in table client
            
            updateTablesNecess(context: context, varEntityName: varEntityName, idUserLogined: idUserLogined, id_retirer: id_retirer, typeCompteSelected: typeCompteSelected, varEntityNameRecu: varEntityNameRecu, typeCompte: typeCompte, varNom: varNom, argentCaculated: argentCaculated )
            
            // reload tableView data which added
            guard let vc = storyboard?.instantiateViewController(identifier: "comptecourant") as? CompteCourantViewController else {
                return
            }
            vc.completionHandler = { [weak self] in
                vc.refresh()
            }
        
            vc.typeComptesPass = typeCompte
            vc.titleNomComptes = varNom
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.title = "Comptes \(varNom) "  + ":" + " \(argentCaculated)"
            navigationController?.pushViewController(vc, animated: true)
            // end reload tableView data which added
        }
    }
    
    func updateArgentClient(argentCaculated: Float, varNom: String, argentCaculatedRecu: Float, varNomRecu: String, managedObject: NSManagedObject) {
        managedObject.setValue(argentCaculated, forKey: "compte_\(varNom)_solde")
        
        // update argent in compte vired
        managedObject.setValue(argentCaculatedRecu, forKey: "compte_\(varNomRecu)_solde")
    }
    
    func updateDataClickOK(argentCaculated: Float, varNom: String, argentCaculatedRecu: Float, varNomRecu: String, managedObject: NSManagedObject, context: NSManagedObjectContext, varEntityName: String, idUserLogined: Int, typeCompteSelected: Int, varEntityNameRecu: String, typeCompte: Int, id_retirer: Int) {
        updateArgentClient(argentCaculated: argentCaculated, varNom: varNom, argentCaculatedRecu: argentCaculatedRecu, varNomRecu: varNomRecu, managedObject: managedObject)
        
        do {
            try context.save()
            print("update successfull")
        } catch let error as NSError {
            print("Could not Update. \(error), \(error.userInfo)")
        }
        
        updateTablesNecess(context: context, varEntityName: varEntityName, idUserLogined: idUserLogined, id_retirer: id_retirer, typeCompteSelected: typeCompteSelected, varEntityNameRecu: varEntityNameRecu, typeCompte: typeCompte, varNom: varNom, argentCaculated: argentCaculated )
    }
    
    func updateTablesNecess(context: NSManagedObjectContext, varEntityName: String, idUserLogined: Int, id_retirer: Int, typeCompteSelected: Int, varEntityNameRecu: String, typeCompte: Int, varNom: String, argentCaculated: Float ) {
        // insert data
        let newCompteCourant = NSEntityDescription.insertNewObject(forEntityName: varEntityName, into: context)
        newCompteCourant.setValue(Float(txtMontant.text ?? "0")!, forKey: "argent")
        newCompteCourant.setValue(idUserLogined, forKey: "client_id")
        newCompteCourant.setValue(Date(), forKey: "date_created")
        newCompteCourant.setValue(id_retirer + 1, forKey: "id")
        newCompteCourant.setValue(1, forKey: "mark")
        newCompteCourant.setValue(typeCompteSelected, forKey: "type_compte")
        newCompteCourant.setValue(false, forKey: "mark_recu")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data
        
        // get data to get id compte recu
        var id_retirer_recu = 0
        let request_recu = NSFetchRequest<NSFetchRequestResult>(entityName: varEntityNameRecu)
        request_recu.returnsObjectsAsFaults = false
        request_recu.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        request_recu.fetchLimit = 1
        do {
            let result = try context.fetch(request_recu) as! [NSManagedObject]
            id_retirer_recu = result[0].value(forKey: "id") as! Int
        }
        catch let error as NSError {
            print("\(error)")
        }
        // get data to get id compte recu
        
        // insert data into compte recu
        let newCompteRecu = NSEntityDescription.insertNewObject(forEntityName: varEntityNameRecu, into: context)
        newCompteRecu.setValue(Float(txtMontant.text ?? "0")!, forKey: "argent")
        newCompteRecu.setValue(idUserLogined, forKey: "client_id")
        newCompteRecu.setValue(Date(), forKey: "date_created")
        newCompteRecu.setValue(id_retirer_recu + 1, forKey: "id")
        newCompteRecu.setValue(1, forKey: "mark")
        newCompteRecu.setValue(true, forKey: "mark_recu")
        newCompteRecu.setValue(typeCompte, forKey: "type_compte")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data into compte recu
    }
    
    func displayMyAlertMessage(userMessage:String) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: nil)

        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    func displayMyConfirmMessage(userMessage:String, appDel: AppDelegate, context: NSManagedObjectContext, idUserLogined: Int, typeCompte: Int, argentCaculated: Float, varNom: String, argentCaculatedRecu: Float, varNomRecu: String, managedObject: NSManagedObject, varEntityName: String, typeCompteSelected: Int, varEntityNameRecu: String, id_retirer: Int) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            if typeCompte == 2 {
                self.closeCompteLivretA(appDel: appDel, context: context, idUserLogined: idUserLogined)
            } else {
                self.closeCompteEpargne(appDel: appDel, context: context, idUserLogined: idUserLogined)
            }
            
            self.updateDataClickOK(argentCaculated: argentCaculated, varNom: varNom, argentCaculatedRecu: argentCaculatedRecu, varNomRecu: varNomRecu, managedObject: managedObject, context: context, varEntityName: varEntityName, idUserLogined: idUserLogined, typeCompteSelected: typeCompteSelected, varEntityNameRecu: varEntityNameRecu, typeCompte: typeCompte, id_retirer: id_retirer)
            
            // return form mes comptes
            guard let vc = self.storyboard?.instantiateViewController(identifier: "cell") as? ClientEspaceViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
            // end return form mes comptes
        })
        let cancel = UIAlertAction(title: "Annuler", style: .cancel, handler: { (action) -> Void in
            
        })
        alert.addAction(okAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)

    }
    
    func closeCompteLivretA(appDel: AppDelegate, context: NSManagedObjectContext, idUserLogined: Int) {
        // update data is_close_compte_livreta
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Client")
        let id_1 = idUserLogined
        let predicate = NSPredicate(format: "id = %i", id_1 as Int)
        fetchRequest.predicate = predicate
        do {
            let  rs = try context.fetch(fetchRequest)
            for _ in rs as [NSManagedObject] {
                // update
                do {
                    let managedObject = rs[0]
                    managedObject.setValue(true, forKey: "is_close_compte_livreta")
                    try context.save()
                    // print("update successfull")
                } catch let error as NSError {
                    print("Could not Update. \(error), \(error.userInfo)")
                }
                //end update
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        // end update data is_close_compte_livreta
    }
    
    func closeCompteEpargne(appDel: AppDelegate, context: NSManagedObjectContext, idUserLogined: Int) {
        // update data is_close_compte_epargne
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Client")
        let id_1 = idUserLogined
        let predicate = NSPredicate(format: "id = %i", id_1 as Int)
        fetchRequest.predicate = predicate
        do {
            let  rs = try context.fetch(fetchRequest)
            for _ in rs as [NSManagedObject] {
                // update
                do {
                    let managedObject = rs[0]
                    managedObject.setValue(true, forKey: "is_close_compte_epargne")
                    try context.save()
                    print("update successfull")
                } catch let error as NSError {
                    print("Could not Update. \(error), \(error.userInfo)")
                }
                //end update
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        // end update data is_close_compte_epargne
    }
    
    @IBAction func didTapReset(_ sender: Any) {
        txtMontant.text = ""
        pickerTextField.text = ""
    }
    
}
