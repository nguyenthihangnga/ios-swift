//
//  ConseillerLoginViewController.swift
//  projet
//
//  Created by Hang Nga on 19/12/2020.
//

import UIKit
import CoreData

class ConseillerLoginViewController: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    
    var arrData = [ConseillerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Espace conseiller"

        // Do any additional setup after loading the view.
        
        // navigation bar
        setupLeftNavItem()
        
        // get user_defaut: marked logined
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "isUserLoggedIn")
        defaults.set(0, forKey: "idUser")
        defaults.set("", forKey: "nomUser")
        defaults.set("", forKey: "prenomUser")
        // end get user_defaut: marked logined

        // no show title for button back
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    @IBAction func didCreateConseiller(_ sender: Any) {
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // get data
        var id = 0
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Conseiller")
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        request.fetchLimit = 1
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            if result.count > 0 {
                id = result[0].value(forKey: "id") as! Int
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
        // end get data to get id
        
        // insert data
        let newCompteCourant = NSEntityDescription.insertNewObject(forEntityName: "Conseiller", into: context)
        newCompteCourant.setValue(id + 1, forKey: "id")
        newCompteCourant.setValue("test", forKey: "password")
        newCompteCourant.setValue("test", forKey: "username")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data
    }
    
    @IBAction func didCreateClient(_ sender: Any) {
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // get data
        var id = 0
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        request.fetchLimit = 1
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            if result.count > 0 {
                id = result[0].value(forKey: "id") as! Int
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
        // end get data to get id
        
        // insert data
        let newClient = NSEntityDescription.insertNewObject(forEntityName: "Client", into: context)
        newClient.setValue(id + 1, forKey: "id")
        newClient.setValue("tester", forKey: "nom")
        newClient.setValue("client", forKey: "prenom")
        newClient.setValue("8 massena 75013", forKey: "adresse")
        newClient.setValue("1234567890", forKey: "telephone")
        newClient.setValue(1, forKey: "conseiller_id")
        newClient.setValue("8007041149605", forKey: "num_compte")
        newClient.setValue(1000, forKey: "compte_courant_solde")
        newClient.setValue(10, forKey: "compte_livreta_solde")
        newClient.setValue(200, forKey: "compte_epargne_solde")
        newClient.setValue(false, forKey: "is_deleted")
        newClient.setValue(false, forKey: "is_close_compte_livreta")
        newClient.setValue(false, forKey: "is_close_compte_epargne")
        newClient.setValue(Date(), forKey: "created_date")
        newClient.setValue(Date(), forKey: "updated_date")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data
        
        // insert data: compte courant
        let newCompteCourant = NSEntityDescription.insertNewObject(forEntityName: "CompteCourant", into: context)
        newCompteCourant.setValue(1, forKey: "id")
        newCompteCourant.setValue(1, forKey: "type_compte")
        newCompteCourant.setValue(2, forKey: "mark")
        newCompteCourant.setValue(id + 1, forKey: "client_id")
        newCompteCourant.setValue(Date(), forKey: "date_created")
        newCompteCourant.setValue(1000, forKey: "argent")
        newCompteCourant.setValue(false, forKey: "mark_recu")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data: compte courant
        
        // insert data: compte livreta
        let newCompteLivreta = NSEntityDescription.insertNewObject(forEntityName: "CompteLivretA", into: context)
        newCompteLivreta.setValue(1, forKey: "id")
        newCompteLivreta.setValue(2, forKey: "type_compte")
        newCompteLivreta.setValue(2, forKey: "mark")
        newCompteLivreta.setValue(id + 1, forKey: "client_id")
        newCompteLivreta.setValue(Date(), forKey: "date_created")
        newCompteLivreta.setValue(10, forKey: "argent")
        newCompteLivreta.setValue(false, forKey: "mark_recu")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data: compte livreta
        
        // insert data: compte epargne
        let newCompteEpargne = NSEntityDescription.insertNewObject(forEntityName: "CompteEpargne", into: context)
        newCompteEpargne.setValue(1, forKey: "id")
        newCompteEpargne.setValue(3, forKey: "type_compte")
        newCompteEpargne.setValue(2, forKey: "mark")
        newCompteEpargne.setValue(id + 1, forKey: "client_id")
        newCompteEpargne.setValue(Date(), forKey: "date_created")
        newCompteEpargne.setValue(200, forKey: "argent")
        newCompteEpargne.setValue(false, forKey: "mark_recu")
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        // end insert data: compte epargne
    }
    
    
    @IBAction func didTapReset(_ sender: Any) {
        txtUserName.text = ""
        txtPassword.text = ""
    }
    
    
    @IBAction func didTapSubmit(_ sender: Any) {
        arrData = ConseillerData.getAllConseillerData()
        
        let lblUserName = txtUserName.text!
        let lblPassword = txtPassword.text!
        
        if (lblUserName.isEmpty) || (lblPassword.isEmpty) {
            displayMyAlertMessage(userMessage: "Vous n'avez pas rempli tous les champs.")
            return
        } else {
            for result in arrData {
                
                let valueUserName = result.titleUserName
                let trimmedValueUserName = valueUserName.trimmingCharacters(in: .whitespacesAndNewlines)
                let trimmedLblUserName = lblUserName.trimmingCharacters(in: .whitespacesAndNewlines)
                
                let valuePassword = result.titlePassword
                let trimmedValuePassword = valuePassword.trimmingCharacters(in: .whitespacesAndNewlines)
                let trimmedLblPassword = lblPassword.trimmingCharacters(in: .whitespacesAndNewlines)
                
                if ((trimmedValueUserName.elementsEqual(trimmedLblUserName)) == true) && ((trimmedValuePassword.elementsEqual(trimmedLblPassword)) == true) {
                
                    UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                    UserDefaults.standard.set(result.titleId, forKey: "idUser")
                    UserDefaults.standard.set(valueUserName, forKey: "username")
                    UserDefaults.standard.set(valuePassword, forKey: "username")
                    UserDefaults.standard.synchronize()
                    
                    // move form continue
                    guard let vc = storyboard?.instantiateViewController(identifier: "client_list") as? ClientListViewController else {
                        return
                    }
                    vc.navigationItem.largeTitleDisplayMode = .never
                    navigationController?.pushViewController(vc, animated: true)
                    // end move form continue
                    
                    break
                    
                } else {
                    displayMyAlertMessage(userMessage: "Il n'y a pas de conseiller avec ces informations")
                    continue
                }
            }
            
        }
    }
    
    func displayMyAlertMessage(userMessage:String) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: nil)

        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
}
