//
//  EditClientViewController.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import UIKit
import CoreData

class EditClientViewController: UIViewController {
    
    public var id_client:Int = 0
    
    @IBOutlet weak var txtPrenom: UITextField!
    @IBOutlet weak var txtNom: UITextField!
    @IBOutlet weak var txtTelephone: UITextField!
    @IBOutlet weak var txtAdresse: UITextField!
    
    var arrData = [ClientDetailModel]()
    
    var idUser:Any = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getDataClient()
        
        // no show title for button back
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // navigation bar
        setupRightNavItemConseiller()
        
        // get user_defaut: marked logined
        let defaults = UserDefaults.standard
        idUser = defaults.value(forKey: "idUser")
        if idUser as! Int == 0 {
            didTapHome()
        }
        // end get user_defaut: marked logined
    }
    
    func getDataClient() {
        arrData = ClientDetailData.getAllClientData(idClient:id_client)
        
        txtAdresse.text = arrData[0].titleAdresse
        txtTelephone.text = arrData[0].titleTelephone
        
        txtNom.text = arrData[0].titleNom
        txtPrenom.text = arrData[0].titlePrenom
        
        // only read
        txtNom.isUserInteractionEnabled = false
        txtPrenom.isUserInteractionEnabled = false
    }
    

    @IBAction func didTapReset(_ sender: Any) {
        txtAdresse.text = arrData[0].titleAdresse
        txtTelephone.text = arrData[0].titleTelephone
    }
    
    
    @IBAction func didTapSave(_ sender: Any) {
        let lblAdresse = txtAdresse.text!
        let lblTelephone = txtTelephone.text!
        
        if (lblAdresse.isEmpty) || (lblTelephone.isEmpty) {
            displayMyAlertMessage(userMessage: "Vous n'avez pas rempli tous les champs.")
            return
        } else {
            // connect db
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
            
            // update data is_deleted
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Client")
            let id_1 = id_client
            let predicate = NSPredicate(format: "id = %i", id_1 as Int)
            fetchRequest.predicate = predicate
            do {
                let  rs = try context.fetch(fetchRequest)
                for result in rs as [NSManagedObject] {
                    // update
                    do {
                        let managedObject = rs[0]
                        managedObject.setValue(txtTelephone.text, forKey: "telephone")
                        managedObject.setValue(txtAdresse.text, forKey: "adresse")
                        try context.save()
                        print("update successfull")
                    } catch let error as NSError {
                        print("Could not Update. \(error), \(error.userInfo)")
                    }
                    //end update
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            // end update data is_deleted
            
            // return page previous
            guard let vc = storyboard?.instantiateViewController(identifier: "client_list") as? ClientListViewController else {
                return
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func displayMyAlertMessage(userMessage:String) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: nil)

        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
}
