//
//  AddClientViewController.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import UIKit
import CoreData

class AddClientViewController: UIViewController {
    
    @IBOutlet weak var txtPrenom: UITextField!
    @IBOutlet weak var txtAdresse: UITextField!
    @IBOutlet weak var txtTelephone: UITextField!
    @IBOutlet weak var txtNom: UITextField!
    
    var idUser:Any = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // navigation bar
        setupRightNavItemConseiller()
        
        // get user_defaut: marked logined
        let defaults = UserDefaults.standard
        idUser = defaults.value(forKey: "idUser")
        if idUser as! Int == 0 {
            didTapHome()
        }
        // end get user_defaut: marked logined
    }
    
    @IBAction func didTapReset(_ sender: Any) {
        txtNom.text = ""
        txtPrenom.text = ""
        txtTelephone.text = ""
        txtAdresse.text = ""
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        let lblNom = txtNom.text!
        let lblPrenom = txtPrenom.text!
        let lblAdresse = txtAdresse.text!
        let lblTelephone = txtTelephone.text!
        
        if (lblNom.isEmpty) || (lblPrenom.isEmpty) || (lblAdresse.isEmpty) || (lblTelephone.isEmpty) {
            displayMyAlertMessage(userMessage: "Vous n'avez pas rempli tous les champs.")
            return
        } else {
            // connect db
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
            
            // begin check client existed or not
            if checkClientExistedOrNot(valueNom: lblNom, valuePrenom: lblPrenom, valueTelephone: lblTelephone, appDel: appDel, context: context) == false {
            // end check client existed or not
            
                // get data to get id
                var id = 0
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
                request.returnsObjectsAsFaults = false
                request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
                request.fetchLimit = 1
                do {
                    let result = try context.fetch(request) as! [NSManagedObject]
                    if result.count > 0 {
                        id = result[0].value(forKey: "id") as! Int
                    }
                }
                catch let error as NSError {
                    print("\(error)")
                }
                // end get data to get id
                
                // insert data
                let newClient = NSEntityDescription.insertNewObject(forEntityName: "Client", into: context)
                newClient.setValue(id + 1, forKey: "id")
                newClient.setValue(txtNom.text, forKey: "nom")
                newClient.setValue(txtPrenom.text, forKey: "prenom")
                newClient.setValue(txtAdresse.text, forKey: "adresse")
                newClient.setValue(txtTelephone.text, forKey: "telephone")
                newClient.setValue(idUser as! Int, forKey: "conseiller_id")
                newClient.setValue("8007041149606", forKey: "num_compte")
                newClient.setValue(1000, forKey: "compte_courant_solde")
                newClient.setValue(10, forKey: "compte_livreta_solde")
                newClient.setValue(200, forKey: "compte_epargne_solde")
                newClient.setValue(false, forKey: "is_deleted")
                newClient.setValue(false, forKey: "is_close_compte_livreta")
                newClient.setValue(false, forKey: "is_close_compte_epargne")
                newClient.setValue(Date(), forKey: "created_date")
                newClient.setValue(Date(), forKey: "updated_date")
                do {
                  try context.save()
                } catch let error as NSError {
                  print("Could not save. \(error), \(error.userInfo)")
                }
                // end insert data
                
                // insert data: compte courant
                let newCompteCourant = NSEntityDescription.insertNewObject(forEntityName: "CompteCourant", into: context)
                newCompteCourant.setValue(1, forKey: "id")
                newCompteCourant.setValue(1, forKey: "type_compte")
                newCompteCourant.setValue(2, forKey: "mark")
                newCompteCourant.setValue(id + 1, forKey: "client_id")
                newCompteCourant.setValue(Date(), forKey: "date_created")
                newCompteCourant.setValue(1000, forKey: "argent")
                newCompteCourant.setValue(false, forKey: "mark_recu")
                do {
                  try context.save()
                } catch let error as NSError {
                  print("Could not save. \(error), \(error.userInfo)")
                }
                // end insert data: compte courant
                
                // insert data: compte livreta
                let newCompteLivreta = NSEntityDescription.insertNewObject(forEntityName: "CompteLivretA", into: context)
                newCompteLivreta.setValue(1, forKey: "id")
                newCompteLivreta.setValue(2, forKey: "type_compte")
                newCompteLivreta.setValue(2, forKey: "mark")
                newCompteLivreta.setValue(id + 1, forKey: "client_id")
                newCompteLivreta.setValue(Date(), forKey: "date_created")
                newCompteLivreta.setValue(10, forKey: "argent")
                newCompteLivreta.setValue(false, forKey: "mark_recu")
                do {
                  try context.save()
                } catch let error as NSError {
                  print("Could not save. \(error), \(error.userInfo)")
                }
                // end insert data: compte livreta
                
                // insert data: compte epargne
                let newCompteEpargne = NSEntityDescription.insertNewObject(forEntityName: "CompteEpargne", into: context)
                newCompteEpargne.setValue(1, forKey: "id")
                newCompteEpargne.setValue(3, forKey: "type_compte")
                newCompteEpargne.setValue(2, forKey: "mark")
                newCompteEpargne.setValue(id + 1, forKey: "client_id")
                newCompteEpargne.setValue(Date(), forKey: "date_created")
                newCompteEpargne.setValue(200, forKey: "argent")
                newCompteEpargne.setValue(false, forKey: "mark_recu")
                do {
                  try context.save()
                } catch let error as NSError {
                  print("Could not save. \(error), \(error.userInfo)")
                }
                // end insert data: compte epargne
            } else {
                self.displayMyAlertMessage(userMessage: "Ce client existe déjà")
            }
        }
        
        // return page previous
        guard let vc = storyboard?.instantiateViewController(identifier: "client_list") as? ClientListViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func displayMyAlertMessage(userMessage:String) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: nil)

        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    func checkClientExistedOrNot(valueNom: String, valuePrenom: String, valueTelephone: String, appDel: AppDelegate, context: NSManagedObjectContext) -> Bool {
        // get data
        let requestSolde = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
        requestSolde.returnsObjectsAsFaults = false
        let predicate = NSPredicate(format: "(nom == %@) AND (is_deleted == %@) AND (prenom == %@) AND (telephone == %@)", "\(valueNom)", NSNumber(value: false), "\(valuePrenom)", "\(valueTelephone)")
        requestSolde.predicate = predicate
        do {
            let result = try context.fetch(requestSolde) as! [NSManagedObject]

            if result.isEmpty {
                return false
            } else {
                return true
            }
           
        }
        catch let error as NSError {
            print("\(error)")
        }
        // end get data
        return true
    }
}
