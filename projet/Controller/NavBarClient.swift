//
//  NavBarClient.swift
//  projet
//
//  Created by Hang Nga on 05/01/2021.
//

import UIKit

extension UIViewController {
    func setupNavigationBarItems() {
        setupLeftNavItem()
        setupRightNavItem()
        // setupRemainingNavItems()
    }
    
    func setupRemainingNavItems() {
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func setupLeftNavItem() {
        let followButtonLeft = UIButton(type: .custom)
        followButtonLeft.setImage(UIImage(named: "home.png"), for: .normal)
        followButtonLeft.frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        followButtonLeft.widthAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.heightAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.addTarget(self, action: #selector(didTapHome), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButtonLeft)
    }
    
    func setupLeftNavItemBankAccount() {
        let followButtonLeft = UIButton(type: .custom)
        followButtonLeft.setImage(UIImage(named: "bank_account.jpeg"), for: .normal)
        followButtonLeft.frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        followButtonLeft.widthAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.heightAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.addTarget(self, action: #selector(didTapClientEspace), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButtonLeft)
    }
    
    func setupLeftNavItemAddClient() {
        let followButtonLeft = UIButton(type: .custom)
        followButtonLeft.setImage(UIImage(named: "add_client.jpeg"), for: .normal)
        followButtonLeft.frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        followButtonLeft.widthAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.heightAnchor.constraint(equalToConstant: 20).isActive = true
        followButtonLeft.addTarget(self, action: #selector(didTapClientAdd), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButtonLeft)
    }
    
    func setupRightNavItem() {
        let followButton = UIButton(type: .custom)
        followButton.setImage(UIImage(named: "logout.jpeg"), for: .normal)
        followButton.frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        followButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        followButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        followButton.addTarget(self, action: #selector(didTapHome), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: followButton)
    }
    
    func setupRightNavItemConseiller() {
        let followButton = UIButton(type: .custom)
        followButton.setImage(UIImage(named: "logout.jpeg"), for: .normal)
        followButton.frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        followButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        followButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        followButton.addTarget(self, action: #selector(didTapHome), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: followButton)
    }
    
    @objc func didTaplogout() {
        guard let vc = storyboard?.instantiateViewController(identifier: "client_login") as? ClientLoginViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTaplogoutConseiller() {
        guard let vc = storyboard?.instantiateViewController(identifier: "conseiller_login") as? ConseillerLoginViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    @objc func didTapHome() {
        guard let vc = storyboard?.instantiateViewController(identifier: "home") as? HomeViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapClientEspace() {
        guard let vc = storyboard?.instantiateViewController(identifier: "cell") as? ClientEspaceViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapClientAdd() {
        guard let vc = storyboard?.instantiateViewController(identifier: "add_client") as? AddClientViewController else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}
