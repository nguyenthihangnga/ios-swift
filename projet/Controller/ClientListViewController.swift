//
//  ClientListViewController.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import UIKit
import CoreData

class ClientListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var sampleData = [ListClientModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Liste des clients"
        
        // get user_defaut: marked logined
        let defaults = UserDefaults.standard
        let idUser = defaults.value(forKey: "idUser")
        if idUser as! Int == 0 {
            didTapHome()
        }
        // end get user_defaut: marked logined
        
        // navigation bar
        setupLeftNavItemAddClient()
        setupRightNavItemConseiller()
      
        sampleData = ListClientData.getAllListClientData()
        
        // no show title for button back
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // Navigation Bar Text: change color for title
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.systemBlue]
    }

}

extension ClientListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ClientListCell
        
        cell?.lblNomPrenom.text = "\(sampleData[indexPath.row].titlePrenom) \(sampleData[indexPath.row].titleNom)"
        
        // click button in cell
        cell!.cellDelegate = self
        cell!.index = indexPath
        
        return cell!
    }
    
    
}

extension ClientListViewController:tableViewNewClient {
    func onClickCell(index: Int) {
        /*
        print ("\(index) clicked")
        print ("\(sampleData[index].titleId as! Int) clicked")
        */
        
        // cloture client
        displayMyAlertMessage(userMessage:"Êtes-vous sûr de vouloir fermer ce compte ?", index: index)
    }
    
    func onClickCloture(index: Int) {
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // update data is_deleted
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Client")
        let id_1 = sampleData[index].titleId
        let predicate = NSPredicate(format: "id = %i", id_1 as Int)
        fetchRequest.predicate = predicate
        do {
            let  rs = try context.fetch(fetchRequest)
            for _ in rs as [NSManagedObject] {
                // update
                do {
                    let managedObject = rs[0]
                    managedObject.setValue(true, forKey: "is_deleted")
                    try context.save()
                    print("update successfull")
                } catch let error as NSError {
                    print("Could not Update. \(error), \(error.userInfo)")
                }
                //end update
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        // end update data is_deleted
    }
    
    func onClickCellEdit(index: Int) {
        guard let vc = storyboard?.instantiateViewController(identifier: "edit_client") as? EditClientViewController else {
            return
        }
        
        let id_client = sampleData[index].titleId as! Int
        
        vc.title = "Modifier le client"
        
        vc.id_client = id_client
        
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func displayMyAlertMessage(userMessage:String, index:Int) {

        let alert = UIAlertController(title: "Alerte!", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "D'accord", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            self.onClickCloture(index: index)
            // reload page
            guard let vc = self.storyboard?.instantiateViewController(identifier: "client_list") as? ClientListViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
       })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)

        alert.addAction(okAction)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil)
    }
}
