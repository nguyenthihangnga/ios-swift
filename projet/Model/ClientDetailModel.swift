//
//  ClientDetailModel.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import Foundation
import UIKit

struct ClientDetailModel {
    let titleNom:String
    let titlePrenom:String
    let titleTelephone:String
    let titleId:Int
    let titleAdresse:String
}
