//
//  ListClientModel.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import Foundation
import UIKit

struct ListClientModel {
    let titleDateCreated:Date
    let titleNom:String
    let titlePrenom:String
    let titleTelephone:String
    let titleAdress:String
    let titleIsDeleted:Bool
    let titleIsCloseCompteLivreta:Bool
    let titleIsCloseCompteEpargne:Bool
    let titleId:Int
}
