//
//  ConseillerData.swift
//  projet
//
//  Created by Hang Nga on 30/12/2020.
//

import Foundation
import UIKit
import CoreData

class ConseillerData{
    
    static func getAllConseillerData() -> [ConseillerModel]{
        var arrData = [ConseillerModel]()
        
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // get data
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Conseiller")
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        do {
            let results = try context.fetch(request)
            
            for result in results as! [NSManagedObject] {
                arrData.append((ConseillerModel(titleUserName: result.value(forKey: "username") as! String, titlePassword: result.value(forKey: "password") as! String, titleId: result.value(forKey: "id") as! Int)))
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
        /*
        arrData = [
        
            ConseillerModel(titleUserName: "test", titlePassword: "test", titleId: 2)
            
        ]
        */
        return arrData
    }
    
}
