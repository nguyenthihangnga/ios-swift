//
//  ClientDetailData.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import Foundation
import UIKit
import CoreData

class ClientDetailData{
    
    static func getAllClientData(idClient:Int) -> [ClientDetailModel]{
        var arrData = [ClientDetailModel]()
        
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // get data
        let requestSolde = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
        requestSolde.returnsObjectsAsFaults = false
        let predicate = NSPredicate(format: "id = %i", idClient)
        requestSolde.predicate = predicate
        do {
            let result = try context.fetch(requestSolde) as! [NSManagedObject]
           
            arrData.append(ClientDetailModel(titleNom: result[0].value(forKey: "nom") as! String, titlePrenom: result[0].value(forKey: "prenom") as! String, titleTelephone: result[0].value(forKey: "telephone") as! String, titleId: result[0].value(forKey: "id") as! Int, titleAdresse: result[0].value(forKey: "adresse") as! String))
        }
        catch let error as NSError {
            print("\(error)")
        }
        // end get data
        
        return arrData
    }
    
}
