//
//  ListClientData.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import Foundation
import UIKit
import CoreData

class ListClientData{
    
    static func getAllListClientData() -> [ListClientModel]{
        
        var arrDataClient = [ListClientModel]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        /*
        let dateFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            return dateFormatter
        }()
        */
        
        // get id User
        let defaults = UserDefaults.standard
        let idUserLogined = defaults.value(forKey: "idUser")
        // end get id User
        
        // connect db
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        
        // get data
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Client")
        request.returnsObjectsAsFaults = false
        
        // many where
        let predicate = NSPredicate(format: "(conseiller_id = %i) AND (is_deleted == %@)", idUserLogined as! Int, NSNumber(value: false))
        request.predicate = predicate
        
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        do {
            let results = try context.fetch(request)
            
            for result in results as! [NSManagedObject] {
                arrDataClient.append(ListClientModel(titleDateCreated: dateFormatter.date(from: "2020-12-29")!, titleNom: result.value(forKey: "nom") as! String, titlePrenom: result.value(forKey: "prenom") as! String, titleTelephone: result.value(forKey: "telephone") as! String, titleAdress: result.value(forKey: "adresse") as! String, titleIsDeleted: result.value(forKey: "is_deleted") as! Bool, titleIsCloseCompteLivreta: result.value(forKey: "is_close_compte_livreta") as! Bool, titleIsCloseCompteEpargne: result.value(forKey: "is_close_compte_epargne") as! Bool, titleId: result.value(forKey: "id") as! Int))
            }
        }
        catch let error as NSError {
            print("\(error)")
        }
        // end get data
        
        return arrDataClient
    }
    
}

