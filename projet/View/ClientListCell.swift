//
//  ClientListCell.swift
//  projet
//
//  Created by Hang Nga on 06/01/2021.
//

import UIKit

protocol tableViewNewClient {
    func onClickCell(index: Int)
    func onClickCellEdit(index: Int)
}

class ClientListCell: UITableViewCell {

    @IBOutlet weak var lblNomPrenom: UILabel!
    @IBOutlet weak var butEdit: UIButton!
    @IBOutlet weak var butCloture: UIButton!
    
    var cellDelegate: tableViewNewClient?
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didTapEdit(_ sender: Any) {
        cellDelegate?.onClickCellEdit(index: index!.row)
    }
    
    @IBAction func didTapCloture(_ sender: Any) {
        cellDelegate?.onClickCell(index: index!.row)
    }
    
}
